# Copyright (c) 2010 Markus Rothe
# Copyright (c) 2013, 2014 Marvin Schmidt <marv@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN}1
MY_PNV=${MY_PN}-${PV}
WORK="${WORKBASE}"/${MY_PNV}

require cmake [ api=2 ]

SUMMARY="A library to talk to FTDI chips"
DESCRIPTION="
A library to talk to FTDI chips: FT232BM/245BM, FT2232C/D and FT232/245R using
libusb, including the popular bitbang mode. This library is linked with your
program in userspace, no kernel driver required.
"
HOMEPAGE="http://www.intra2net.com/en/developer/libftdi/index.php"
DOWNLOADS="http://www.intra2net.com/en/developer/libftdi/download/${MY_PNV}.tar.bz2"

LICENCES="( GPL-2 LGPL-2 )"
SLOT="1"
PLATFORMS="~amd64"
MYOPTIONS="cxx [[ description = [ Build libftdipp, a wrapper library providing C++ bindings ] ]]"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libusb:1
        cxx? ( dev-libs/boost )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    '-DLIB_SUFFIX='
    '-DBUILD_TESTS=FALSE'
    '-DDOCUMENTATION=FALSE'
    '-DEXAMPLES=FALSE'
    '-DFTDI_EEPROM=FALSE'
    '-DPYTHON_BINDINGS=FALSE'
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'cxx FTDIPP'
)

src_install() {
    cmake_src_install

    option cxx || edo rm "${IMAGE}"/usr/$(exhost --target)/lib/pkgconfig/libftdipp1.pc
}

